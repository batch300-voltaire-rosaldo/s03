<?php

class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is $this->firstName $this->lastName";
    }
}

class Developer extends Person{

    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }

}

class Engineer extends Person{
    public function printName(){
        return "Your are an engineer $this->firstName $this->middleName $this->lastName.";
    }

}

$person1 = new Person("Senku", "Senpai", "Ishigami");
$person2 = new Developer("John", "Finch", "Smith");
$person3 = new Engineer("Harold", "Myers", "Reese");

class Character {

	public $name;

	public function __construct($name){
		$this->name = $name;
	}

    public function printName(){
        return "Hi, I am $this->name!";
    }

}

class VoltesMember extends Character{

	public $vehicle;

	public function __construct($name,$vehicle){
		parent::__construct($name);
		$this->vehicle = $vehicle;
	}

    public function printName(){
        return "Hi, I am $this->name! I pilot the Volt $this->vehicle!";
    }

}

$voltes1 = new VoltesMember("Steve", "Cruiser");
$voltes2 = new VoltesMember("Big Bert", "Panzer");
$voltes3 = new VoltesMember("Little John", "Frigate");
$voltes4 = new VoltesMember("Jamie", "Lander");
$voltes5 = new VoltesMember("Mark", "Bomber");
