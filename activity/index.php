<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03 Activity</title>
</head>
<body>
    <h1>Person</h1>
    <p><?= $person1->printName()?></p>

    <h1>Developer</h1>
    <p><?= $person2->printName()?></p>

    <h1>Engineer</h1>
    <p><?= $person3->printName()?></p>

    <h1>Voltes V</h1>
    <p><?php var_dump($voltes1)?></p>
    <p><?php var_dump($voltes2)?></p>
    <p><?php var_dump($voltes3)?></p>
    <p><?php var_dump($voltes4)?></p>
    <p><?php var_dump($voltes5)?></p>

    <p><?= $voltes1->printName()?></p>
    <p><?= $voltes2->printName()?></p>
    <p><?= $voltes3->printName()?></p>
    <p><?= $voltes4->printName()?></p>
    <p><?= $voltes5->printName()?></p>

</body>
</html>