<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Classes and Objects</title>
</head>
<body>
    
    <h1>Objects from Variable</h1>
    <!-- <p><?php echo $buildingObj->name; ?></p>
    <p><?php echo $buildingObj->address -> city; ?></p>
    <p><?= print_r($buildingObj) ?></p> -->


    <p><?php echo $buildingObj2->address; ?></p>

    <h1>Objects from Classes</h1>
    <p><?php var_dump($building)?></p>

    <h2>Modify the Instantiated Object</h2>
    <?php $building->name = "GMA Network";?>
    <?php $building->floors = "Twenty-one";?>
    <p><?php var_dump($building)?></p>
    <p><?= $building->printName()?></p>


    <h1>Inheritance (Condominium Object)</h1>

    <p><?php var_dump($condominium)?></p>
    <p><?= $condominium->name;?></p>
    <p><?= $condominium->floors;?></p>
    <p><?= $condominium->address;?></p>

    <h1>Polymorphism</h1>
    <p><?= $condominium->printName()?></p>

    <!-- Mini Activity -->
    <h1>Pokemon</h1>
    <p><?php var_dump($pokemon1)?></p>
    <p><?php var_dump($pokemon2)?></p>
    <p><?php var_dump($pokemon3)?></p>
    <p><?= $pokemon3->attack("Bulbasaur");?></p>

    <p><?php var_dump($dog1)?></p>

</body>
</html>